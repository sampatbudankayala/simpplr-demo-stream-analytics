### Prerequisite to Run Confluent Kafka With Docker :

* `Download docker from the below URL based on platform and Install`
   
   [DOCKER Download Link](https://docs.docker.com/docker-for-windows/install/ "Site link to download and setup docker on any platform")
   

### How to Run Docker Commands using a "yaml" File :

`In order to run docker commands we can use the docker-compose file and run the commands. For the following Kafka usecase we will be using` **LANDOOP** `image to kick start kafka with schema registry`.

> Below is "docker-compose.yml" we will be using to create a Confluent Kafka server 


```
version: '2'

services:
  # this is our kafka cluster.
  kafka-cluster:
    image: landoop/fast-data-dev:cp3.3.0
    environment:
      ADV_HOST: 127.0.0.1         # Change to 192.168.99.100 if using Docker Toolbox
      RUNTESTS: 0                 # Disable Running tests so the cluster starts faster
      FORWARDLOGS: 0              # Disable running 5 file source connectors that bring application logs into Kafka topics
      SAMPLEDATA: 0               # Do not create sea_vessel_position_reports, nyc_yellow_taxi_trip_data, reddit_posts topics with sample Avro records.
    ports:
      - 2181:2181                 # Zookeeper
      - 3030:3030                 # Landoop UI
      - 8081-8083:8081-8083       # REST Proxy, Schema Registry, Kafka Connect ports
      - 9581-9585:9581-9585       # JMX Ports
      - 9092:9092                 # Kafka Broker

```

### Command to run Docker using compose yaml File :

```
1) Change the directory , to the location where [docker-compose.yml] file present. 
2) Then run below commands : 
```
 `Command to run on Terminal`:   **docker-compose up**
 
**NOTE:** `The above command will now create the Kafka Cluster and dowloads all the required images for the Kafka server.`

### How to Verify the Kafka Server is Up ?

`Click on to the "Kafka Server URL" to verify`

[KAFKA Server Url ](https://localhost:3030 "This URL directs to Landoop UI of Kafka Server")

**NOTE : Once the Kafka Server is up we need to download Kafka tools from the Confluent for handling schema Registry. In order setup the tool below are two ways that can be acieved :**

```
# Two options to get access to the confluent tools

# Option 1:
# Download the confluent binaries at:
# https://www.confluent.io/download/
# Put them on your system and put the confluent/bin directory in your path
kafka-avro-console-consumer.sh

# Option 2:
# Use a docker image to have access to all the binaries right away:

docker run -it --rm --net=host confluentinc/cp-schema-registry:3.3.1 bash
# Then you can do 
kafka-avro-console-consumer
```







