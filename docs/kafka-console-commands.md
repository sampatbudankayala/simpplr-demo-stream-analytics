### Start Kafka Server

```cd /usr/lib/kafka```

```bin/kafka-server-start.sh config/server.properties```


### How to create a topic in Kafka with Name "fist_topic".

```kafka-topics.sh --zookeeper 127.0.0.1:2181 --topic first_topic --create --partitions 3 --replication-factor 1```


### How to list all the available topics.
```kafka-topics.sh --zookeeper 127.0.0.1:2181 --list```

 ```first_topic```
 
### How to get the metadata information of a topic.

```kafka-topics.sh --zookeeper 127.0.0.1:2181 --topic first_topic --describe```

<table class="Topic_Describe">
<caption>Caption: Sample Description</caption>
<tr>
<th>first_topic</th>
<th>PartitionCount:3</th>
<th>ReplicationFactor:1</th>
<th>Configs</th>
</tr>
<tr>
<td>first_topic</td>
<td>Partition: 0</td>
<td>Leader: 0</td>
<td>Replicas: 0 || Isr: 0</td>
</tr>
<tr>
<td>first_topic</td>
<td>Partition: 1</td>
<td>Leader: 0</td>
<td>Replicas: 0 || Isr: 0</td>
</tr>
<tr>
<td>first_topic</td>
<td>Partition: 2</td>
<td>Leader: 0</td>
<td>Replicas: 0 || Isr: 0</td>
</tr>
</table>


### How to start a console Producer.

```kafka-console-producer.sh --broker-list 127.0.0.1:9092 --topic first_topic --producer-property acks=all```

### How to start a console Consumer.

```kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic first_topic --from-beginning```

***Consuming messages over a group***

```kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic first_topic --group my-first-application```

### How to list all the available Consumer Groups.

```kafka-consumer-groups.sh --bootstrap-server 127.0.0.1:9092 --list```
       
   * my-first-application
   
### How to describe the Consumer Group Metadata Info .
```kafka-consumer-groups  --bootstrap-server 127.0.0.1:9092 --describe --group my-first-application```

<table class="Consumer_Describe">
<caption>Caption:Sample Consumer Group Description</caption>
<tr>
<th>Topic</th>
<th>Partition</th>
<th>Current-Offset</th>
<th>Log-End-Offset</th>
<th>Log</th>
<th>Consumer-id</th>
<th>Host</th>
<th>Client-ID</th>
</tr>
<tr>
<td>first_topic</td>
<td>0</td>
<td>11</td>
<td>11</td>
<td>0</td>
<td>-</td>
<td>-</td>
<td>-</td>
</tr>
<tr>
<td>first_topic</td>
<td>2</td>
<td>13</td>
<td>13</td>
<td>0</td>
<td>-</td>
<td>-</td>
<td>-</td>
</tr>
<tr>
<td>first_topic</td>
<td>1</td>
<td>10</td>
<td>10</td>
<td>0</td>
<td>-</td>
<td>-</td>
<td>-</td>
</tr>
</table>

### How to Replay my data from a particular offset for a Consumer "Ressting the Consumer Offset"

```kafka-consumer-groups  --bootstrap-server 127.0.0.1:9092 --describe --group my-first-application --reset-offsets --to-earliest --execute --topic first_topic```

<table class="Reset_offset">
<caption>Caption: Sample Reset Offset</caption>
<tr>
<th>topic</th>
<th>Partition</th>
<th>New_Offset</th>
</tr>
<tr>
<td>first_topic</td>
<td>0</td>
<td>0</td>
</tr>
<tr>
<td>first_topic</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<td>first_topic</td>
<td>2</td>
<td>0</td>
</tr>
</table>