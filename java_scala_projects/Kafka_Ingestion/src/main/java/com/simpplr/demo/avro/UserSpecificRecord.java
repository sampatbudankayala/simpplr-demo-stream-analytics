package com.simpplr.demo.avro;

import com.simpplr.demo.UserEvolution;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class UserSpecificRecord {

    private final static Logger LOGGER =
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public UserEvolution getJsonFromRest(String input_url) {

        String inline = "";
        UserEvolution.Builder userRecordBuilder = UserEvolution.newBuilder();

        try {
            URL url = new URL(input_url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            int response = connection.getResponseCode();
            LOGGER.log(Level.INFO, "Response Code -> " + response);

            if (response != 200) {
                throw new RuntimeException("HttpResponseCode " + response);
            } else {
                Scanner sc = new Scanner(url.openStream());
                while (sc.hasNext()) {
                    inline += sc.nextLine();
                }
                //Close the stream when reading the data has been finished
                sc.close();

            }

            JSONParser parse = new JSONParser();
            JSONObject jobj = (JSONObject) parse.parse(inline);
            JSONArray jsonarr_1 = (JSONArray) jobj.get("results");

            for (int i = 0; i < jsonarr_1.size(); i++) {
                //Store the JSON objects in an array
                //Get the index of the JSON object and print the values as per the index
                JSONObject jsonobj_1 = (JSONObject) jsonarr_1.get(i);
                JSONObject jsonobj_2 = (JSONObject) jsonobj_1.get("name");
                JSONObject jsonobj_3 = (JSONObject) jsonobj_1.get("location");
                JSONObject jsonobj_3_1 = (JSONObject) jsonobj_3.get("coordinates");
                JSONObject jsonobj_3_2 = (JSONObject) jsonobj_3.get("timezone");
                JSONObject jsonobj_4 = (JSONObject) jsonobj_1.get("login");
                JSONObject jsonobj_5 = (JSONObject) jsonobj_1.get("dob");
                JSONObject jsonobj_6 = (JSONObject) jsonobj_1.get("registered");

                userRecordBuilder.setGender(jsonobj_1.get("gender").toString());
                userRecordBuilder.setGender(jsonobj_1.get("gender").toString());
                userRecordBuilder.setNameTitle(jsonobj_2.get("title").toString());
                userRecordBuilder.setNameFirst(jsonobj_2.get("first").toString());
                userRecordBuilder.setNameLast(jsonobj_2.get("last").toString());
                userRecordBuilder.setLocationCity(jsonobj_3.get("city").toString());
                userRecordBuilder.setLocationState(jsonobj_3.get("state").toString());
                userRecordBuilder.setLocationPostcode(Integer.parseInt(jsonobj_3.get("postcode").toString()));
                userRecordBuilder.setLocationCoordinatesLatitude(Double.parseDouble(jsonobj_3_1.get("latitude").toString()));
                userRecordBuilder.setLocationCoordinatesLongitude(Double.parseDouble(jsonobj_3_1.get("longitude").toString()));
                userRecordBuilder.setLocationTimezoneOffset(jsonobj_3_2.get("offset").toString());
                userRecordBuilder.setLocationTimezoneDescription(jsonobj_3_2.get("description").toString());
                userRecordBuilder.setEmail(jsonobj_1.get("email").toString());
                userRecordBuilder.setLoginUsername(jsonobj_4.get("username").toString());
                userRecordBuilder.setLoginUuid(jsonobj_4.get("uuid").toString());
                userRecordBuilder.setDobDate(jsonobj_5.get("date").toString());
                userRecordBuilder.setDobAge(Integer.parseInt(jsonobj_5.get("age").toString()));
                userRecordBuilder.setRegisteredDate(jsonobj_6.get("date").toString());
                userRecordBuilder.setPhone(jsonobj_1.get("phone").toString());
                userRecordBuilder.setCell(jsonobj_1.get("cell").toString());
                userRecordBuilder.setNat(jsonobj_1.get("nat").toString());

            }


        } catch (ProtocolException ex) {
            ex.printStackTrace();
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return userRecordBuilder.build();

    }


}


