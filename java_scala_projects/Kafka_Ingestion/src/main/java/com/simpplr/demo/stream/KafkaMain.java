package com.simpplr.demo.stream;

public class KafkaMain {

    public static void main(String[] args) {

        boolean isAsync = args.length == 0 || !args[0].trim().equalsIgnoreCase("sync");

        KafkaAvroProducer producer = new KafkaAvroProducer("first_topic",isAsync,KafkaAvroConstants.REST_PROXY_URL);
        producer.start();

    }
}
