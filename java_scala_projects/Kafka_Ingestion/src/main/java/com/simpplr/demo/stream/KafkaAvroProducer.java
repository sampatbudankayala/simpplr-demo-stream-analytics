package com.simpplr.demo.stream;

import com.simpplr.demo.UserEvolution;
import com.simpplr.demo.avro.UserSpecificRecord;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class KafkaAvroProducer extends Thread {

    private final KafkaProducer<String, UserEvolution> producer;
    private final String topic;
    private final boolean isAsync;
    private final String restProxyURL;

    UserSpecificRecord usr = new UserSpecificRecord();

    public KafkaAvroProducer(String topic, boolean isAsync, String restProxyURL) {

        Properties props = new Properties();

        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaAvroConstants.KAFKA_SERVER_URL + ":" + KafkaAvroConstants.KAFKA_SERVER_PORT);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "Avro_Producer");
        //props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
       // props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, KafkaAvroConstants.MAX_FLIGHT_CONNECTION_VALUE);
        props.put(ProducerConfig.ACKS_CONFIG, KafkaAvroConstants.ACKS_VALUE);
        props.put(ProducerConfig.RETRIES_CONFIG, KafkaAvroConstants.RETRY_VALUE);
        props.put(ProducerConfig.LINGER_MS_CONFIG, KafkaAvroConstants.LINGER_VALUE);
        props.put(ProducerConfig.BATCH_SIZE_CONFIG, KafkaAvroConstants.MSG_BATCH_VALUE);
        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, KafkaAvroConstants.KAFKA_PRODUCER_BUFFER_SIZE);
              // avro part
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        props.setProperty("schema.registry.url","http://127.0.0.1:8081");

        this.producer = new KafkaProducer<>(props);
        this.topic = topic;
        this.isAsync = isAsync;
        this.restProxyURL = restProxyURL;
        System.out.println("Inside constructor");

    }

    public void sendMessage(String key, UserEvolution value) {
        long startTime = System.currentTimeMillis();
        if (isAsync) {
            //Send Asynchronously
            System.out.println("Sending Asynchronously");
            producer.send(new ProducerRecord<String, UserEvolution>(topic, key, value),
                    (Callback) new CallBackImplementation(startTime, key, value));
        } else {
            //Send Synchronously
            try {
                producer.send(
                        new ProducerRecord<String, UserEvolution>(topic, key, value))
                        .get();
                System.out.println("Sent Message: (" + key + ", " + value + ")");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }

    }

    public void run() {
        System.out.println("Inside Run");
        int messageNo = 0;
        while (true) {
            try {
                UserEvolution line = null;
                while (true) {
                    line = usr.getJsonFromRest(restProxyURL);
                    System.out.println("Line test -> " + line);
                    messageNo++;
                    this.sendMessage(messageNo + "", line);
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    System.out.println("Inside the Try Block");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class CallBackImplementation implements Callback {
        private long startTime;
        private String key;
        private Object message;

        public CallBackImplementation(long startTime, String key, Object message) {
            this.startTime = startTime;
            this.key = key;
            this.message = message;
        }

        public void onCompletion(RecordMetadata recordMetadata, Exception e) {
            long elapsedTime = System.currentTimeMillis() - startTime;
            // executes every time a record is successfully sent or an exception is thrown
            if (e == null) {
                System.out.println("[SUCCESS] : message(" + key + ", " + message
                        + ") sent to partition(" + recordMetadata.partition() + "), "
                        + "offest(" + recordMetadata.offset() + ") in " + elapsedTime
                        + " ms");
            } else if (recordMetadata != null) {
                System.out.println("[METADATA_FAILED] : message(" + key + ", " + message
                        + ") sent to partition(" + recordMetadata.partition() + "), "
                        + "offest(" + recordMetadata.offset() + ") in " + elapsedTime
                        + " ms");

            } else {
                System.out.println("[PRINTING STACK TRACE");
                e.printStackTrace();
            }

        }
    }


}
