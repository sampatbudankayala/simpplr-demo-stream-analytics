package com.simpplr.demo.stream;

public interface KafkaAvroConstants {

    public static final String TOPIC1 = "first_topic";
    public static final String KAFKA_SERVER_URL = "http://127.0.0.1";
    public static final int KAFKA_SERVER_PORT = 9092;
    public static final int KAFKA_PRODUCER_BUFFER_SIZE = 64 * 1024;
    public static final String CLIENT_ID = "kafka_client";
    public static final String ACKS_VALUE = "all";
    public static final int RETRY_VALUE = 2;
    public static final int LINGER_VALUE = 1;
    public static final int MSG_BATCH_VALUE = 2000;
    public static final int MAX_FLIGHT_CONNECTION_VALUE = 2;
    public static final String REST_PROXY_URL = "https://randomuser.me/api/?&noinfo";


}
