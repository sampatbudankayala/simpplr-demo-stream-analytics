package org.simpplr.test


import com.simpplr.config.SparkApplication
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{Column, DataFrame, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming._
import org.apache.spark.sql.types._
import org.simpplr.config.SparkContextInitializer
import org.simpplr.logparser.CloudTrailLogParserMain.sparkAppName


import org.apache.spark.sql.functions.{lit, schema_of_json, from_json}
import collection.JavaConverters._


object Test extends SparkApplication {
  override def sparkAppName: String = "CLoud-Trail-Parser"

  def main(args: Array[String]): Unit = {

    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)

    val additionOptions = List(("spark.local.dir", "/tmp"))


    val sc = SparkContextInitializer.createSparkContext(sparkAppName, additionOptions)

    val spark: SparkSession = SparkSession.builder().config(sc.getConf).getOrCreate()


    val readData = spark.read.parquet("file:///Users/sampatbudankayala/Downloads/r02.parquet")

    readData.printSchema()
    readData.show()

    import org.apache.spark.sql.functions.from_json
    import spark.implicits._

   // val json_schema = spark.read.json(readData.select("detail").as[String]).schema

    //json_schema.printTreeString()
    //readData.withColumn("jsonData", from_json($"detail", json_schema)).select("jsonData.*").select(col ="requestparameters.currentdeliverystreamversionid").show()



  }
}