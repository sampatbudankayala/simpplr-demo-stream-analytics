package org.simpplr.logparser


import com.simpplr.config.SparkApplication
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{Column, DataFrame, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.streaming._
import org.apache.spark.sql.types._
import org.simpplr.config.SparkContextInitializer


object CloudTrailLogParserMain extends SparkApplication {
  override def sparkAppName: String = "CLoud-Trail-Parser"

  def main(args: Array[String]): Unit = {

    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)


    val additionOptions = List(("spark.local.dir", "/tmp"))

    val sc = SparkContextInitializer.createSparkContext(sparkAppName, additionOptions)

    val spark: SparkSession = SparkSession.builder().config(sc.getConf).getOrCreate()

    val cloudTrailLogsPath = "file:///Users/sampatbudankayala/AWSdata/*/*/*/*.json.gz"
    val parquetOutputPath = "file:///Users/sampatbudankayala/AWSdataparquett/" // DBFS or S3 path

    val schemaObject = new SimpplrSchemaObjects()

    val cloudTrailSchema = schemaObject.aws_cloudtrail_log_schema

    val rawRecords: DataFrame = spark.read
      .schema(cloudTrailSchema)
      .json(cloudTrailLogsPath)

    import spark.implicits._

    val cloudTrailEvents = rawRecords
      .select(explode($"Records") as "record")
      .select(
        unix_timestamp($"record.eventTime", "yyyy-MM-dd'T'hh:mm:ss").cast("timestamp") as "timestamp",
        $"record.*")

    val flatCloudTrailSchema = SchemaHelpers.flattenDataFrame(cloudTrailEvents)


    flatCloudTrailSchema.show()


  }

}
