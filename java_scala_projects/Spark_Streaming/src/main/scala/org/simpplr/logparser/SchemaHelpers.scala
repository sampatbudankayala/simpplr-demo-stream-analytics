package org.simpplr.logparser

import org.apache.spark.sql.Column
import org.apache.spark.sql.types.{ArrayType, StructField, StructType}
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Column
import org.apache.spark.sql.functions.col

object SchemaHelpers {

  def flattenSchema(schema: StructType, prefix: String = null): Array[Column] = {
    schema.fields.flatMap(f => {
      val colName = if (prefix == null) f.name else (prefix + "." + f.name)
      f match {
        case StructField(_, struct: StructType, _, _) => flattenSchema(struct, colName)
        case StructField(_, ArrayType(x: StructType, _), _, _) => flattenSchema(x, colName)
        case StructField(_, ArrayType(_, _), _, _) => Array(col(colName))
        case _ => Array(col(colName))
      }
    })
  }

  def flattenDataFrame(df: DataFrame): DataFrame = {
    df.select(flattenSchema(df.schema): _*)
  }


  def normalise_field(raw: String) = {
    raw.trim.replace("`", "")
      .replace("-", "_")
      .replace(":", "")
      .replace(" ", "_")
      .toLowerCase
  }


}
